$(document).ready(function () {

	var pause_selection = false
	var select_rule_type = "Nielsen's 10 Heuristic Principles"

	//set evaluation_in_progress to false initially 
	if (localStorage.getItem("evaluation_in_progress") === null) {
		localStorage.setItem("evaluation_in_progress", false)
	}


	//close dashboard 
	window.tray_already_opened = false;
	chrome.runtime.onMessage.addListener(
		function (request, sender, sendResponse) {
			if (request.greeting == "please close the tray") {

				// Close the tray, remove all DOM elements and function handlers
				$('iframe.ux-netsol').remove();
				$('.ux_overlay').remove();
				$("body").css("cssText", "margin-left: 0 !important; width: 100% !important; position:absolute !important; cursor: auto !important;");
			}
			if (request.greeting == "please open the tray") {
				if ($('#ux_tray').length == 0) {
					if (pause_selection == false)
						window.hover_enabled = true; // Are we in 'hover mode'?
					var margin_top = 0; // some websites are dumb and add margin-top to body
					var doc_height = Math.max(
						$(document).height(),
						$(window).height()
					);
					var doc_width = Math.max(
						$(document).width(),
						$(window).width()
					) - 274;
					// ========= UI INJECTION =========

					// Scoot <body> over to the right by 274px
					$("body").css("cssText", "margin-left: 274px !important; width: 100% !important; position:absolute !important; cursor: pointer !important;");

					// Inject tray html template into body (but it's fixed)
					var trayUrl = chrome.runtime.getURL('tray.html');


					//Open the tray in Iframe
					var container = $("<iframe class='ux-netsol' style='border:none;width: 298px;height:100%;position:fixed;left:0;top:0;z-index:2000000050;'></iframe>")
					$("body").prepend(container)

					container.get(0).contentWindow.document.open();
					container.get(0).contentWindow.document.write("<html><body><div id='add' class='ux_reset ux-netsol' data-omglol='yo'></div></body></html>");
					container.get(0).contentWindow.document.close();

					container = $(container.get(0).contentWindow.document)
					//Append css to iframe head
					var href = chrome.runtime.getURL('assets/css/bootstrap.min.css');
					var head_element = jQuery('iframe.ux-netsol').contents().find("head")
					head_element.append(`<link type="text/css" rel="stylesheet" href="` + href + `" media="all" />`)



					var href1 = chrome.runtime.getURL('assets/css/style.css');
					var head_element = jQuery('iframe.ux-netsol').contents().find("head")
					head_element.append(`<link type="text/css" rel="stylesheet" href="` + href1 + `" media="all" />`)

					//Append Fonts

					var styleNode = document.createElement("style");
					styleNode.type = "text/css";
					styleNode.textContent = "@font-face { font-family: futura_md_btmedium; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_medium_bt-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_medium_bt-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futura_md_btbold_italic; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_bold_italic_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_bold_italic_font-webfont.woff") + "') format('woff'); }" +


						"@font-face { font-family: futura_bdcn_btbold; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_bold_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_bold_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futurabook; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_book_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_book_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futura_bk_btbook_italic; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_book_italic_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_book_italic_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futura_xblk_btextra_black; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_extra_black_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_extra_black_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futuraheavy; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_heavy_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_heavy_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futura_hv_btheavy_italic; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_heavy_italic_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_heavy_italic_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futura_ltcn_btlight; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_light_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_light_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futuralight_italic; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_light_italic_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_light_italic_font-webfont.woff") + "') format('woff'); }" +

						"@font-face { font-family: futuramedium_italic; font-weight: normal; font-style: normal ; src: url('" +
						chrome.extension.getURL("assets/fonts/futura_medium_italic_font-webfont.woff2") + "') format('woff2'), " +
						"url('" + chrome.extension.getURL("assets/fonts/futura_medium_italic_font-webfont.woff") + "') format('woff'); }"


					var cln = styleNode.cloneNode(true);
					document.head.appendChild(styleNode);
					head_element.append(cln)
					//font appending finished

					container.find("#add").load(trayUrl, function () {

						//load Images  in tray
						var header_img = chrome.runtime.getURL('assets/images/ux-audit.jpg')
						container.find('#ux-header').attr('src', header_img)

						var down_img = chrome.runtime.getURL('assets/images/down.svg')
						container.find('#down-arrow').attr('src', down_img)

						var report_img = chrome.runtime.getURL('assets/images/report.svg')
						container.find('#report-img').attr('src', report_img)

						var current_progress_img = chrome.runtime.getURL('assets/images/current_progress.svg')
						container.find('#current_progress_img').attr('src', current_progress_img)

						var settings_img = chrome.runtime.getURL('assets/images/rules.svg')
						container.find('#setting-img').attr('src', settings_img)

						var up_img = chrome.runtime.getURL('assets/images/up.svg')
						container.find('#up-img').attr('src', up_img)

						var star_img = chrome.runtime.getURL('assets/images/star.svg')
						container.find('.star-img').attr('src', star_img)

						var close_img = chrome.runtime.getURL('assets/images/close.svg')
						container.find('.close-img').attr('src', close_img)

						//keep state of audit button
						if (localStorage.getItem('evaluation_in_progress') != 'false') {
							container.find('.start-audit').children('span').html('End Audit')
							container.find('.start-audit').attr('title', 'End Audit')
							activateHighlighting();
						} else {
							container.find('.start-audit').children('span').html('Start Audit')
							container.find('.start-audit').attr('title', 'Start Audit')
							hide_overlays()
							window.hover_enabled = false
						}
						// Hide current progress and pause selection if audit has not started
						if (localStorage.getItem('current_report_name') == 'null' || localStorage.getItem('current_report_name') == null) {
							container.find('#ux_audit_current_progress').hide();
							container.find('.pause_selection').hide();
						}
						container.find(".down-arrow-1").hide();
						container.find(".down-arrow-1").click(function () {
							container.find(".left-navigation.links").toggleClass("hight-expand");
							$(this).hide();
							container.find(".up-arrow-1").show();
						});
						container.find(".up-arrow-1").click(function () {
							container.find(".left-navigation.links").removeClass("hight-expand");
							$(this).hide();
							container.find(".down-arrow-1").show();
						});
						container.find('#collapseOnes, #collapseTwos').slideUp(1);
						container.find('#collapseOne').click(function () {
							container.find('#collapseOnes').slideToggle(500);
							$(this).toggleClass('collapsed');
						})
						container.find('#collapseTwo').click(function () {
							container.find('#collapseTwos').slideToggle(500);
							$(this).toggleClass('collapsed');
						})
						//hide custom rule tile in tray if no custom rules are there
						chrome.storage.local.get('ux_custom_rules', function (result) {
							if (result.ux_custom_rules == undefined || result.ux_custom_rules.length < 1) {
								container.find('#headingTwo').hide();
							}
						})

						// Disable selection if pause button is clicked
						function executePause() {
							// If the callout is up, don't do anything
							if ($('#he_callout').css('display') == 'none') {
								// Reverse hover_enabled flag
								if (window.hover_enabled == false) { // Unpause
									window.hover_enabled = true;
									pause_selection = false
									container.find('.pause_selection').html('Pause Audit')
									container.find('.pause_selection').attr('title', 'Pause Audit')
									$('.ux_overlay').show();
								} else { // Pause
									pause_selection = true
									window.hover_enabled = false;
									$('.ux_overlay').hide();
									$(".ux_overlay").css({
										'height': ''
									});
									container.find('.pause_selection').html('Resume Audit')
									container.find('.pause_selection').attr('title', 'Resume Audit')
								}
							}
						}
						// Execute pause for Ctrl + Shift + P
						$(window).keydown(function (event) {
							if (event.ctrlKey && event.keyCode == 80) {
								event.preventDefault();
								executePause();
							}
						});
						// Execute pause on click of pause button
						container.find(".pause_selection").click(function (e) {
							executePause();
						});
						//toggle state of Start Audit button
						container.find(".start-audit").click(function (event) {
							if ($(this).children('span').html() === 'Start Audit') {
								$("iframe.ux-netsol").width('100%')
								container.find('#myModal').modal('show')
								setTimeout(function () {
									container.find('#audit_name').focus();
								}, 500);

							} else {
								if (stopEvaluation(localStorage.getItem('current_report_name'), true)) {
									window.hover_enabled = false
									container.find('.start-audit').children('span').html('Start Audit')
								}
							}

						});

						//Save name of a audit and save report with that name Also start the Audit 
						container.find('#save_audit_name').click(function (e) {
							var re_audit_name = /^(?=.*[a-zA-Z])\S{0,}$/

							if (!re_audit_name.test($('#audit_name').val())) {
								container.find('#audit-error').show()
								container.find('#audit-error').html('Audit name should not contain any spaces')
							} else {
								// Load previously saved Reports data
								chrome.storage.local.get('saved_reports_data', function (result) {
									if (result.saved_reports_data == undefined) {
										var saved_reports_data = [];
										var saved_report_id = 0;
									} else {
										var saved_reports_data = result.saved_reports_data;
										var saved_report_id = Object.keys(result.saved_reports_data).length;
									}
									// Unpack results

									if (saved_reports_data.find(x => x.audit_name == container.find('#audit_name').val())) {
										container.find('#audit-error').show()
										container.find('#audit-error').html('Audit with the same name already exists.')
									} else {
										container.find('#audit-error').hide()
										localStorage.setItem('current_report_name', container.find('#audit_name').val());
										localStorage.setItem('evaluation_in_progress', true);
										container.find('#myModal').modal('hide')
										//Add data

										// Save to new report to local storage if previous is done
										date = new Date()
										today_date = date.toLocaleDateString()

										saved_reports_data[saved_report_id] = {
											"audit_name": localStorage.getItem('current_report_name'),
											"audit_date": today_date
										};
										chrome.storage.local.set({
											'saved_reports_data': saved_reports_data
										});

										if (container.find('.start-audit').children('span').html() === 'Start Audit') {
											container.find('.start-audit').children('span').html('End Audit')
											container.find('.start-audit').attr('title', 'End Audit')
											if (pause_selection == false)
												window.hover_enabled = true
											$("iframe.ux-netsol").width(298)
											activateHighlighting();
										} else {
											container.find('.start-audit').children('span').html('Start Audit')
											container.find('.start-audit').attr('title', 'Start Audit')
											$("iframe.ux-netsol").width("100%")
										}
									}
								}); //end of saving data to storage

								container.find('#ux_audit_current_progress').show()
								container.find('.pause_selection').show();
							}
						});
						$('#ux_heuristic_select_type').on('change', function () {


							select_rule_type = this.value

							chrome.storage.local.get('ux_custom_rules', function (result) {
								if (result.ux_custom_rules != undefined) {
									var ux_custom_rules = result.ux_custom_rules
								}
								chrome.storage.local.get('ux_settings_saved_data', function (result) {
									// Unpack results
									if (result.ux_settings_saved_data != undefined) {
										var ux_settings_saved_data = result.ux_settings_saved_data;
									}
									populateHeuristicsInSidePane(ux_settings_saved_data, ux_custom_rules);
								});
							});
						});
						container.find('#he_tray_close').click(function (e) {
							chrome.runtime.sendMessage({
								greeting: "close_button_clicked"
							}, function (response) {});
							// Close the tray, remove all DOM elements and function handlers
							jQuery('iframe.ux-netsol').remove();
							$('#he_callout').remove();
							$('.ux_overlay').remove();
							$("body").css("cssText", "margin-left: 0 !important; width: 100% !important; position:absolute !important; overflow:scroll !important; cursor: auto !important;");
						});

						// On click of "All Reports", show reports
						container.find('#ux_audit_all_reports').click(function (e) {
							// Hide the overlays/callout if they're visible
							$("iframe.ux-netsol").width("100%")
							close_callout();
							hide_overlays();
							$('html').css('overflow', 'hidden')
							// Load previously saved data
							chrome.storage.local.get('saved_reports_data', function (result) {
								// Unpack results
								if (result.saved_reports_data == undefined) {
									var saved_reports_data = [];

								} else {
									var saved_reports_data = result.saved_reports_data;
								}
								container.find('#ux_all_reports').show();
								// Generate the table
								// Generate the table
								var main_div = ` <div class="row">
													<div class="col-xs-12 top-header">
													<h1>All Reports</h1>` +
									'<button type="button" class="close close-all-reports" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="' + chrome.runtime.getURL('assets/images/close.svg') + '" alt="close"></span></button>' +
									`</div>
												</div>
												<div class="listing-header hidden-xs hidden-sm">
													<!-- listing-header -->
													<div class="col-md-4 listing-header-name Screenshots">
													<h3>Name</h3>
													</div>
													<div class="col-md-4 text-center listing-header-name">
													<h3>Date Created</h3>
													</div>
													<div class="col-md-4 text-right listing-header-name">
													<h3>Actions</h3>
													</div>
													<!-- /listing-header -->
												</div>`

								function generateTable(saved_reports_data) {
									for (var i = 0; i < saved_reports_data.length; i++) {
										var insert_div = '<div class="listing-data">' +
											'<div title="Open Report Details" class="col-md-4 col-xs-12 listing-data-entry bold-font Screenshots report-details" id="' + saved_reports_data[i]['audit_name'] + '" data-column="Name">';
										insert_div = insert_div + saved_reports_data[i]['audit_name'] + "</div>";
										insert_div = insert_div + '<div class="col-md-4 text-center col-xs-12 listing-data-entry" data-column="Date Created"> <span class="medium-font">';
										insert_div = insert_div + saved_reports_data[i]['audit_date'] + "</div>";
										insert_div = insert_div + "</span>" +
											'<div class="col-md-4 text-right col-xs-12 listing-data-entry" data-column="Actions">' +
											'<span class="hamburger"><img src="' + chrome.runtime.getURL('assets/images/hamburger.svg') + '" alt="hamburger"/></span>' +
											'<span class="cta-btn"><img title="Download Report" class="evaluate" data-attr="' + saved_reports_data[i]['audit_name'] + '"  src="' + chrome.runtime.getURL('assets/images/download.png') + '" alt="audit"/>' +
											'<img data-attr="' + saved_reports_data[i]['audit_name'] + '" title="Delete Report" class="ux_delete_a_report" id="' + i + '"  src="' + chrome.runtime.getURL('assets/images/delete.svg') + '" alt="delete"/></span></div></div></div>'
										main_div = main_div + insert_div
									}
									return main_div;
								}
								// Paint the table
								var insert_div = generateTable(saved_reports_data);
								if (saved_reports_data.length > 0) {
									container.find('#ux_all_reports').empty();
									container.find('#ux_all_reports').html(insert_div);
								} else {
									container.find('#ux_all_reports').html(main_div);
									var empty_html = '<div class="container-fluid content"><div class="row"><div class="col-xs-12"><div class="center text-center empty-screen">' +
										'<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/report-new.svg') + '" alt="report" >' +
										'<h2>No Audit Reports Yet.</h2><h3>They will appear here once you start auditing</h3></div></div></div></div>'
									container.find('#ux_all_reports').append(empty_html);
								}
								container.find(".listing-data").hover(function (e) {
									e.stopImmediatePropagation()
									if ($(this).children('div')[0].id != localStorage.getItem('current_report_name')) {
										$(this).toggleClass("result_hover");
									} else
										$(this).css('pointer', 'default')
								});

								// On click of a Report , show current session progress
								container.find('.report-details').click(function (e) {
									report_id = $(this).attr('id')
									openReportDetails(report_id)

								}); // end onclick of  view_progress
								container.find(".close-all-reports").click(function () {
									$('iframe.ux-netsol').width(298)
									container.find("#ux_all_reports").css('display', 'none')
									$('html').css('overflow', 'scroll')
									container.find('#he_nothing_here').remove();
								})
								//close dashboard	
								container.find('.evaluate').click(function () {
									stopEvaluation($(this).data('attr'), false)
								})


								// Delete annotation click
								container.find('.ux_delete_a_report').click(function () {

									e.preventDefault();
									report_id = $(this).data('attr')
									// Remove the row from the saved data, save data back
									var index = $(this).attr('id');
									var r = window.confirm("Are you sure you want to delete this ? This cannot be undone.");
									if (r == true) {
										if (index > -1) {
											saved_reports_data.splice(index, 1);
										}

										chrome.storage.local.set({
											'saved_reports_data': saved_reports_data
										})

										// Change the id of every other Delete button that 
										// comes after the one we're going to delete
										$(this).parent().parent().parent().nextAll().find('img').each(function () {
											var new_id = $(this).attr("id") - 1;
											$(this).attr("id", new_id);
										})
										// Delete the row
										$(this).parent().parent().parent().remove();
										//Show ":( nothing here"
										chrome.storage.local.get('saved_reports_data', function (result) {
											// Unpack results
											if (result.saved_reports_data == undefined) {
												var saved_reports_data = [];
											} else {
												var saved_reports_data = result.saved_reports_data;
											}

											if (saved_reports_data.length == 0) {
												var empty_html = '<div class="container-fluid content"><div class="row"><div class="col-xs-12"><div class="center text-center empty-screen">' +
													'<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/report-new.svg') + '" alt="report" >' +
													'<h2>No Audit Reports Yet.</h2><h3>They will appear here once you start auditing</h3></div></div></div></div>'
												container.find('#ux_all_reports').append(empty_html);
												container.find('.start-audit').children('span').html('Start Audit')
												hide_overlays()
												window.hover_enabled = false
											}
										});

										// delete respective data from all screenshots
										chrome.storage.local.get('saved_data', function (result) {
											delete result.saved_data[report_id]
										})
									}


								}); // end delete annotation
							});


						});

						container.find('#ux_audit_current_progress').click(function (e) {
							$("iframe.ux-netsol").width("100%")
							$('html').css('overflow', 'hidden')
							report_id = localStorage.getItem('current_report_name');
							openReportDetails(report_id)

						}); // end onclick of  current progress
						//OPen a report's detail on based of its ID
						function openReportDetails(report_id) {
							close_callout();
							hide_overlays();
							container.find("#ux_all_reports").css('display', 'none')
							// Load previously saved data
							chrome.storage.local.get('saved_data', function (result) {
								// Unpack results
								if (result.saved_data == undefined) {
									var saved_data = [];
								} else {
									if (result.saved_data[report_id] == undefined) {
										var saved_data = [];
									} else {
										var saved_data = result.saved_data;
									}
								}
								container.find('#us_audit_board_progress').show();
								// Generate the table
								// Generate the table
								var main_div = `<div class="row">
																	<div class="col-xs-12 top-header">` +
									'<h1>' + report_id + '</h1>' +
									'<button type="button" class="close close-current-session" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="' + chrome.runtime.getURL('assets/images/close.svg') + '" alt="close"></span></button>' +
									`</div>
																</div>
																<div class=" seven-cols listing-header hidden-xs hidden-sm">
																	<!-- listing-header -->
																	<div class="col-md-2 listing-header-name Screenshots">
																	<h3>Screenshots</h3>
																	</div>
																	<div class="col-md-2 listing-header-name">
																	<h3>Hueristics</h3>
																	</div>
																	<div class="col-md-2 listing-header-name">
																	<h3>Severity</h3>
																	</div>
																	<div class="col-md-2 listing-header-name">
																	<h3>Observations</h3>
																	</div>
																	<div class="col-md-3 listing-header-name">
																	<h3>Recommendations</h3>
																	</div>
																	<div class="col-md-1 listing-header-name">
																	<h3>Action</h3>
																	</div>
																	<!-- /listing-header -->
																</div>`

								function generateTable(saved_data) {
									for (var i = 0; i < saved_data.length; i++) {
										var insert_div = "<div class='seven-cols listing-data'>" +
											"<div class='col-md-2  col-xs-12 listing-data-entry Screenshots' data-column='Screenshots'>";
										insert_div = insert_div + "<img  width='150' class='ux_screenshot_preview' src='" + saved_data[i]['screenshot'] + "'/></div>";
										insert_div = insert_div + '<div class="col-md-2 col-xs-12 listing-data-entry" data-column="Hueristics">' +
											'<span class="medium-font">' + saved_data[i]['heuristic'] + '</span></div>';
										insert_div = insert_div + '<div class="col-md-2 col-xs-12 listing-data-entry" data-column="Severity">' +
											'<span class="medium-font">' + saved_data[i]['severity'] + '</span></div>';
										insert_div = insert_div + '<div  class="col-md-2 col-xs-12 listing-data-entry bold-font" data-column="Observations">';
										insert_div = insert_div + saved_data[i]['notes'] + '</div>' +
											'<div class="col-md-3 col-xs-12 listing-data-entry bold-font" data-column="Recommendations">';
										insert_div = insert_div + saved_data[i]['recommendation'] + '</div>' +
											'<div class="col-md-1 col-xs-12 listing-data-entry" data-column="Actions">' +
											'<span class="hamburger"><img title="Delete" data-report_id="' + report_id + '" class="ux_delete_a_record" id="' + i + '" src="' + chrome.runtime.getURL('assets/images/delete.svg') + '" alt="delete"/></span> ' +
											// '<span class="cta-btn"><img data-report_id="' + report_id + '" class="ux_delete_a_record" id="' + i + '" src="' + chrome.runtime.getURL('assets/images/delete.svg') + '" alt="delete"/></span>' +
											'</div>  </div></div>'
										main_div = main_div + insert_div
									}
									return main_div;
								}
								// Paint the table

								if (Object.keys(saved_data).length > 0 && (report_id in saved_data) && saved_data[report_id].length > 0) {
									var insert_div = generateTable(saved_data[report_id]);
									container.find('#us_audit_board_progress').empty();
									container.find('#us_audit_board_progress').html(insert_div);
								} else {
									container.find('#us_audit_board_progress').html(main_div);
									var empty_html = '<div class="container-fluid content"><div class="row"><div class="col-xs-12"><div class="center text-center empty-screen">' +
										'<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/report-new.svg') + '" alt="report" >' +
										'<h2>No Audited Screens Yet.</h2><h3>They will appear here once you start auditing</h3></div></div></div></div>'
									container.find('#us_audit_board_progress').append(empty_html);
								}

								// Close the View Progress table
								container.find('.close-current-session').click(function () {
									$('iframe.ux-netsol').width(298)
									container.find('#us_audit_board_progress').hide();
									container.find('#he_nothing_here').remove();
									$('html').css('overflow', 'scroll')
									if (pause_selection == false)
										window.hover_enabled = true;
								});

								// Screenshot Preview Open
								container.find('.ux_screenshot_preview').click(function () {
									// Open it	

									var screenshot_src = $(this).attr('src');
									var window_height = $(window).height();

									var progress_table_html = '<div id="screenshot_preview_container">' +
										'<img id="he_progress_close" width="17" unselectable="true" src="' + chrome.runtime.getURL("assets/images/close.svg") + '" />' +
										'<img id="ux_screenshot_preview" src="' +
										screenshot_src +
										'" /></div>';

									container.find('#add').append(progress_table_html);
									var screenshot_height = container.find('#ux_screenshot_preview').height();
									var screenshot_top = (window_height - screenshot_height) / 2;
									container.find('#ux_screenshot_preview').css('margin-top', screenshot_top);

									// Hook up close functionality
									container.find('#screenshot_preview_container').click(function () {
										container.find('#screenshot_preview_container').remove();
									});
								});

								// Delete a report click
								container.find('.ux_delete_a_record').click(function () {
									// Remove the row from the saved data, save data back
									var index = $(this).attr('id');
									var report_id = $(this).data('report_id')
									var r = window.confirm("Are you sure you want to delete this ? This cannot be undone.");
									if (r == true) {
										if (index > -1) {
											saved_data[report_id].splice(index, 1);
										}

										chrome.storage.local.set({
											'saved_data': saved_data
										})

										// Change the id of every other Delete button that 
										// comes after the one we're going to delete
										$(this).parent().parent().parent().nextAll().find('img').each(function () {
											var new_id = $(this).attr("id") - 1;
											$(this).attr("id", new_id);
										})

										// Delete the row
										$(this).parent().parent().parent().remove();

										//Show ":( nothing here"
										chrome.storage.local.get('saved_data', function (result) {

											// Unpack results
											if (result.saved_data == undefined) {
												var saved_data = [];

											} else {
												var saved_data = result.saved_data;

											}
											if (saved_data[report_id].length == 0) {
												var empty_html = '<div class="container-fluid content"><div class="row"><div class="col-xs-12"><div class="center text-center empty-screen">' +
													'<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/report-new.svg') + '" alt="report" >' +
													'<h2>No Audited Screens Yet.</h2><h3>They will appear here once you start auditing</h3></div></div></div></div>'
												container.find('#us_audit_board_progress').append(empty_html);
											}
										});
									}


								}); // end delete annotation

							});
						} //end open report function

						// On click of "Settings" open settings
						container.find('#ux_open_settings').click(function (e) {

							$('iframe.ux-netsol').width('100%')
							$('html').css('overflow', 'hidden')
							// Hide the overlays/callout if they're visible
							close_callout();
							hide_overlays();

							// Load previously saved data
							chrome.storage.local.get('ux_settings_saved_data', function (result) {
								var ux_settings_saved_data = result.ux_settings_saved_data;

								// Create an overlay

								container.find('#ux_settings').show();

								//Append neilsen rules in settings
								neilsen_rules = ux_settings_saved_data.heuristics_sets.find((obj => {
									return obj.set_name === "Nielsen's 10 Heuristic Principles"
								})).heuristics
								var li = '';
								for (var i in neilsen_rules) {
									li = li + '<li><div class="rule-content"><h3>' +
										neilsen_rules[i].title + '</h3><p>' +
										neilsen_rules[i].details + '</p></div></li>'
								}
								// append lis to ul 
								container.find('.neilsen-rules-ul').html(li);

								// Show custom rules 
								chrome.storage.local.get('ux_custom_rules', function (result) {
									if (result.ux_custom_rules == undefined) {
										var ux_custom_rules = []
									} else {
										var ux_custom_rules = result.ux_custom_rules;
									}
									$('.neilsen-rules-custom-ul').html('')
									var li = '';
									for (var i in ux_custom_rules.reverse()) {
										li = li + '<li class="result_hover"><div class="rule-content"><h3>' +
											ux_custom_rules[i].title + '</h3><p>' +
											ux_custom_rules[i].details + '</p></div><div class="cta-btn-div">' +
											// '<span class="hamburger"><img src="' + chrome.runtime.getURL('assets/images/hamburger.svg') + '" alt="hamburger"></span> ' +
											'<span class="cta-btn"><a title="delete" href="javascript:void(0);">' +
											'<img data-attr="' + ux_custom_rules[i].title + '" class="delete_custom_nelsion_rule" src="' + chrome.runtime.getURL('assets/images/delete.svg') + '" alt="delete"></a></span></div></li>'
										// if in case edit needs to be done
										//<a data-toggle="modal" data-target="#edit-rule-modal" class="edit-rule" data-title="'+ ux_custom_rules[i].title +'" data-details="'+ ux_custom_rules[i].details +'" href="javascript:void(0);" title="edit"><img src="'+ chrome.runtime.getURL('assets/images/edit.svg') + '" alt="audit"></a>
									}
									container.find('.neilsen-rules-custom-ul').html(li)

									if (ux_custom_rules.length < 1) {
										container.find('.neilsen-rules-custom-ul').html(`<div class="container-fluid content">
																<div class="row">
																  <div class="col-xs-12">
																  <div class="center text-center empty-screen">` + '<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/norule.svg') + '" alt="report" ></img>' + `
																   <h2>No Custom Rules</h2>
																   <h4>Add your own audit rules</h4>
																   </div>
																  </div>
																</div>
															  </div>`)
									}

									// delete custom nelson rule
									container.find('.delete_custom_nelsion_rule').unbind().click(function (e) {
										e.stopImmediatePropagation();
										var this_d = $(this)
										var r = window.confirm("Are you sure you want to delete this ? This cannot be undone.");

										if (r == true) {
											// Remove the row from the saved data, save data back
											chrome.storage.local.get('ux_custom_rules', function (result) {
												if (result.ux_custom_rules == undefined) {
													var ux_custom_rules = []
												} else {
													var ux_custom_rules = result.ux_custom_rules;
												}
												var index = ux_custom_rules.findIndex(x => x.title == this_d.data('attr'));
												if (index > -1) {
													ux_custom_rules.splice(index, 1);
												}

												chrome.storage.local.set({
													'ux_custom_rules': ux_custom_rules
												})
												if (ux_custom_rules.length < 1) {
													container.find('.neilsen-rules-custom-ul').html(`<div class="container-fluid content">
																	<div class="row">
																	  <div class="col-xs-12">
																	  <div class="center text-center empty-screen">` + '<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/norule.svg') + '" alt="report" ></img>' + `
																	   <h2>No Custom Rules</h2>
																	   <h4>Add your own audit rules</h4>
																	   </div>
																	  </div>
																	</div>
																  </div>`)
													container.find('#headingTwo').hide();
												}
												// Delete the row
												this_d.parents('li').remove();
											}); // end delete annotation
										}

									})

								})
								// on hover of li 

								container.find(".settings-side-nav ul a").click(function () {
									container.find('.settings-side-nav ul a').removeClass('active');
									$(this).addClass('active');
								});


								//$(".blu-shade-2").hide();

								container.find("#rule").click(function () {
									container.find(".blu-shade").hide();
									container.find(".blu-shade-2").show();
								});

								container.find("#rule1").click(function () {
									container.find(".blu-shade").show();
									container.find(".blu-shade-2").hide();
								});

								// Close the settings tab
								container.find('.ux_settings_close').click(function () {
									if (pause_selection == true)
										window.hover_enabled = false
									else
										window.hover_enabled = true
									$('iframe.ux-netsol').width(298)
									container.find('#ux_settings').hide();
									$('html').css('overflow', 'scroll')
								});

							}); //end chrome.storage.local.get callback

							//save new rule
							container.find('#save-new--nelsion-rule').unbind().click(function (e) {
								e.stopImmediatePropagation()
								var title = container.find('#rule_name').val()
								var details = container.find('#rule_detail').val()
								if (title == '' || details == '') {
									container.find('#rule-error').show()
									container.find('#rule-error').html('Rule name or Rule detail cannot be blank.')
								} else {
									var rule_name_regex = /[-!$%^&*()_+|~=`{}["':;<>?,.@#\]]/g;
									if (rule_name_regex.test(title)) {
										container.find('#rule-error').show()
										container.find('#rule-error').html('Rule name cannot contain special characters.')
									} else {
										chrome.storage.local.get('ux_custom_rules', function (result) {
											if (result.ux_custom_rules == undefined) {
												var ux_custom_rules = []
											} else {
												var ux_custom_rules = result.ux_custom_rules
											}
											if (ux_custom_rules.find(x => x.title == title)) {
												container.find('#rule-error').show()
												container.find('#rule-error').html('Rule with the same name already exists.')
											} else {
												var new_rule = {
													"title": title,
													"details": details,
												}
												container.find('#rule-error').hide()
												container.find('#add-rule-modal').hide()
												$('body').removeClass('modal-open');
												$('.modal-backdrop').remove();

												$('#custom-principle').show()
												container.find('#headingTwo').show();
												ux_custom_rules.push(new_rule)

												if (ux_custom_rules.length == 1)
													AddNelsionRule(title, details, container, false)
												else
													AddNelsionRule(title, details, container, true)

												chrome.storage.local.set({
													'ux_custom_rules': ux_custom_rules
												});

												chrome.storage.local.get('ux_settings_saved_data', function (result) {
													// Unpack results
													if (result.ux_settings_saved_data != undefined) {
														var ux_settings_saved_data = result.ux_settings_saved_data;
													}
													populateHeuristicsInSidePane(ux_settings_saved_data, ux_custom_rules);
												});
											}
										})
									}
								}
								if (pause_selection == true)
									window.hover_enabled = false
								else
									window.hover_enabled = true
							});

							//Add nelsion rule to settings HTML
							function AddNelsionRule(title, details, container, render_empty) {
								if (!render_empty)
									container.find('.neilsen-rules-custom-ul').html('')
								var li = '';
								li = li + '<li class="result_hover"><div class="rule-content"><h3>' +
									title + '</h3><p>' +
									details + '</p></div><div class="cta-btn-div">' +
									// '<span class="hamburger"><img src="' + chrome.runtime.getURL('assets/images/hamburger.svg') + '" alt="hamburger"></span> ' +
									'<span class="cta-btn"><a title="delete" href="javascript:void(0);">' +
									'<img data-attr="' + title + '" class="delete_custom_nelsion_rule" src="' + chrome.runtime.getURL('assets/images/delete.svg') + '" alt="delete"></a></span></div></li>'

								container.find('.neilsen-rules-custom-ul').prepend(li)
								container.find('#rule_name').val('')
								container.find('#rule_detail').val('')
								// delete custom nelson rule
								container.find('.delete_custom_nelsion_rule').unbind().click(function () {
									e.stopImmediatePropagation();
									var this_d = $(this)
									var r = window.confirm("Are you sure you want to delete this ? This cannot be undone.");
									if (r == true) {
										// Remove the row from the saved data, save data back
										chrome.storage.local.get('ux_custom_rules', function (result) {
											if (result.ux_custom_rules == undefined) {
												var ux_custom_rules = []
											} else {
												var ux_custom_rules = result.ux_custom_rules;
											}
											var index = ux_custom_rules.findIndex(x => x.title == this_d.data('attr'));
											if (index > -1) {
												ux_custom_rules.splice(index, 1);
											}

											chrome.storage.local.set({
												'ux_custom_rules': ux_custom_rules
											})
											if (ux_custom_rules.length < 1) {
												container.find('.neilsen-rules-custom-ul').html(`<div class="container-fluid content">
																<div class="row">
																  <div class="col-xs-12">
																  <div class="center text-center empty-screen">` + '<img class="d-inline-block" src="' + chrome.runtime.getURL('assets/images/norule.svg') + '" alt="report" ></img>' + `
																   <h2>No Custom Rules</h2>
																   <h4>Add your own audit rules</h4>
																   </div>
																  </div>
																</div>
															  </div>`)
												container.find('#headingTwo').hide();
											}
											// Delete the row
											this_d.parents('li').remove();
										}); // end delete annotation
									}

								})
							}
						}); // end onclick of  open settings

						// On click of a Report , show current session progress
						container.find('.report-details').click(function (e) {
							report_id = $(this).attr('id')
							openReportDetails(report_id)

						}); // end onclick of  view_progress

						container.find('.add-rule-modal-open').click(function () {
							container.find('#add-rule-modal').modal('show')
							container.find('#add-rule-modal').show()
						})


					});

					//Activate highlighting of elements
					function activateHighlighting() {
						$("body").find("*").not("iframe.ux-netsol > #add").not("iframe.ux-netsol.myModal").not("iframe.ux-netsol.add-rule-modal").not("iframe.ux-netsol > #ux_settings").mouseenter(function () {
							if (window.hover_enabled) {
								//calculate dimensions/position of hovered element
								var position = $(this).offset();
								position.left = position.left - 284;
								position.top = position.top - 10;
								var width = $(this).outerWidth() + 20;
								var height = $(this).outerHeight() + 20;

								// Update the overlays to the hovered element
								// Account for sites that put margin-top on body
								position.top = position.top - margin_top;
								$('#hover_overlay_top').css({
									"top": (0 - margin_top) + "px",
									"left": "0",
									"width": "100%",
									"height": (position.top + margin_top) + "px"
								});
								$('#hover_overlay_left').css({
									"top": position.top + "px",
									"left": "0",
									"width": position.left + "px",
									"height": height + "px"
								});
								$('#hover_overlay_right').css({
									"top": position.top + "px",
									"left": position.left + width + "px",
									"width": (doc_width + 274 - position.left - width) + "px",
									"height": height + "px"
								});
								$('#hover_overlay_bottom').css({
									"top": (position.top + height) + "px",
									"left": "0",
									"width": "100%",
									"height": (doc_height - position.top - height) + "px"
								});
							}
						});
					} //end funtion highlighting

					// Overlay Div creation
					$("body").append("<div class='ux_overlay' id='hover_overlay_top'    style='left:0;top:0;width:0;height:0'></div>");
					$("body").append("<div class='ux_overlay' id='hover_overlay_left'   style='left:0;top:0;width:0;height:0'></div>");
					$("body").append("<div class='ux_overlay' id='hover_overlay_right'  style='left:0;top:0;width:0;height:0'></div>");
					$("body").append("<div class='ux_overlay' id='hover_overlay_bottom' style='left:0;top:0;width:0;height:0'></div>");

					//Load previously saved data to insert heuristics
					chrome.storage.local.get('ux_settings_saved_data', function (result) {
						// Unpack results
						if (result.ux_settings_saved_data == undefined) {
							var ux_settings_saved_data = [];
							var new_settings = true;
						} else {
							var ux_settings_saved_data = result.ux_settings_saved_data;
						}
						// Set default data if no data exists
						if (ux_settings_saved_data["overlay_color"] == undefined) {
							var ux_settings_saved_data = {
								"overlay_color": "black",
								"heuristics_sets": [{
										"set_name": "Nielsen's 10 Heuristic Principles",
										"active": true,
										"heuristics": [{
												"title": "Visibility of system status",
												"details": "The system should keep users informed through appropriate feedback within reasonable time"
											},
											{
												"title": "Match between system and the real world",
												"details": "The system should speak the users' language rather than system-oriented terms. Follow real-world conventions"
											},
											{
												"title": "User control and freedom",
												"details": "Users often make mistakes and need 'emergency exits' to leave the unwanted state. Support undo and redo"
											},
											{
												"title": "Consistency and standards",
												"details": "Users shouldn't have to wonder whether different words, situations, or actions mean the same thing. Follow platform conventions"
											},
											{
												"title": "Error prevention",
												"details": "Prevent problems from occuring in the first place, or check for them and present users with a confirmation option before they commit to the action"
											},
											{
												"title": "Recognition rather than recall",
												"details": "Minimize memory load by making objects, actions, and options visible. Instructions should be visible or easily retrievable"
											},
											{
												"title": "Flexibility and efficiency of use",
												"details": "Accelerators - unseen by the novice user - may often speed up the interaction for the expert user. Allow users to tailor frequent actions"
											},
											{
												"title": "Aesthetic and minimalist design",
												"details": "Dialogues should not contain information which is irrelevant or rarely needed"
											},
											{
												"title": "Help recognize & recover from errors",
												"details": "Error messages should be expressed in plain language, indicate the problem, and suggest a solution"
											},
											{
												"title": "Help and documentation",
												"details": "Any necessary help documentation should be easy to search, focused on the user's task, list concrete steps to be carried out, and not be too large"
											}
										]
									},
									{
										"set_name": "Google Mobile App UX Principles",
										"active": false,
										"heuristics": []
									}
								]
							}
						}

						$('.ux_overlay').css({
							'background-color': 'rgba(0,0,0,0.5)'
						});

						if (new_settings) {
							chrome.storage.local.set({
								'ux_settings_saved_data': ux_settings_saved_data
							});
						}
						// Pull out the list of heuristics
						chrome.storage.local.get('ux_custom_rules', function (result) {
							if (result.ux_custom_rules == undefined) {
								var ux_custom_rules = []
							} else {
								var ux_custom_rules = result.ux_custom_rules
							}
							populateHeuristicsInSidePane(ux_settings_saved_data, ux_custom_rules);
						})
					}); // end rules populating

					//var website_name = window.location.host;
					var webpage_title = document.title;
					var webpage_url = window.location;

					//Callout creation or modal creation
					$("body").append('<div id="he_callout" class="modal-body ux-netsol">' +
						'<button type="button" id="he_callout_cancel" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="' + chrome.runtime.getURL('assets/images/close.svg') + '" alt="close"></span></button>' +
						`<div class="block">
						  <p>Select Principle Type</p>
						  <select name="" id="ux_heuristic_select_type">
						  <option>Nielsen's 10 Heuristic Principles</option>
						  <option id='custom-principle'> Custom UX Principles</option>
						  </select>
						</div>
						<div class="block">
						  <p>Add a Principle</p>
						  <select name="" id="ux_heuristic_select">
						  </select>
						</div>
						<div class="block">
						  <p>Severity Level</p>
						  <select name="" id="ux_severity_dropdown">
							<option>0</option>
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						  </select>
						</div>
						<div class="block">
						  <p>Observations</p>
						  <textarea name="" id="ux_observations" cols="30" rows="10"></textarea>
						</div>
				  
						<div class="block">
						  <p>Recommendations</p>
						  <textarea name="" id="ux_recommendation" cols="30" rows="10"></textarea>
						</div>
						<div class="cta-action">
							<span class="save">
							<button type="button" id = "he_callout_save" class="btn btn-primary save-heuristic">SAVE</button>
							</span>
							<span class="ux-cancel">
							<button type="button" id="he_callout_cancel" class="ux-btn btn btn-primary save-heuristic">CANCEL</button>
							</span>
					   </div>
					  </div>`);
					chrome.storage.local.get('ux_custom_rules', function (result) {
						if (result.ux_custom_rules == undefined || result.ux_custom_rules.length < 1) {
							$('#custom-principle').hide()
						}
					}); // end appending a callout
					// If the tray hasn't already been loaded, then we can attach handlers to everything. 
					// Otherwise handlers will already exist
					if (window.tray_already_opened == false) {
						window.tray_already_opened = true;
						//Friendly alert on window resize
						var delay = (function () {
							var timer = 0;
							return function (callback, ms) {
								clearTimeout(timer);
								timer = setTimeout(callback, ms);
							};
						})();

						var havent_alerted_resize = true;

						$(window).resize(function () {
							if ($('#ux_tray').length != 0) {
								if (havent_alerted_resize) {
									delay(function () {
										var r = window.confirm("Sorry, I don't do so well when the window is resized. Can you refresh me?");
										if (r == true) {
											location.reload();
										}
										havent_alerted_resize = false;
									}, 500);
								}
							}
						});

						// ========= ELEMENT SELECTION =========
						// When hovering over the website...
						// Keep hovering effect on if evaluation is in progress
						// Hide the overlay if hovering over the pane
						container.find('#add').mouseenter(function (e) {
							if (window.hover_enabled) {
								hide_overlays();
							}
						})
						// Callout open on click
						$("body").find("*:not(#add):not(#he_callout *):not(#he_callout)").click(function (e) {
							// Only do stuff if selection isn't paused

							if (window.hover_enabled) {
								// Stop the event from bubbling to the website
								e.preventDefault();
								e.stopImmediatePropagation();
								// Do other stuff when a click happens
								$('body').css('cursor', 'default');
								window.hover_enabled = false;
								// Show callout and calculate width and height
								$('#he_callout').css({
									'left': '-1000',
									'top': '-1000',
									'display': 'block'
								});
								var callout_height = $('#he_callout').outerHeight();
								var callout_width = $('#he_callout').outerWidth();
								// Callout --> Calculate coordinates
								var hole_position_right = parseInt($('#hover_overlay_right').css('left').replace(/[^-\d\.]/g, ''));
								var hole_position_top = parseInt($('#hover_overlay_right').css('top').replace(/[^-\d\.]/g, '')); //$('#hover_overlay_right').offset().top - $(window).scrollTop();
								var hole_position_left = parseInt($('#hover_overlay_left').css('width').replace(/[^-\d\.]/g, ''));
								var hole_height = parseInt($('#hover_overlay_left').css('height').replace(/[^-\d\.]/g, ''));
								var hole_position_bottom = hole_position_top + hole_height;

								var window_height = $(window).height();
								var window_width = $(window).width() - 274;

								// === Callout vertical alignment ===

								// If there's enough room on the bottom
								if (window_height - hole_position_bottom > callout_height) {
									// Set callout vertical position to top of hole
									var callout_top = hole_position_top;
									var vertically_bottom = true;
								}

								// If there's not enough room on the bottom but enough room on the top
								else if (hole_position_top - $(window).scrollTop() > callout_height) {
									// Set callout vertical position to bottom of hole minus callout height so it's bottom aligned with hole
									var callout_top = hole_position_bottom - callout_height;
									var vertically_top = true;
								}
								// If there's not enough room on top or bottom
								else {
									// Put callout vertically in the middle
									var callout_top = (window_height - callout_height) / 2 + $(window).scrollTop();
								}
								// === Callout horizontal alignment ===
								// If there's enough room on the right
								if (window_width - hole_position_right > callout_width + 20) {
									// Set callout horizontal position to right of hole
									var callout_left = hole_position_right + 20;
								}

								// If there's not enough room on the right but enough room on the left
								else if (hole_position_left > callout_width + 20) {
									// Set callout vertical position to left of hole minus callout width so it's right aligned to the hole
									var callout_left = hole_position_left - callout_width - 20;
								}

								// If there's not enough room on left or right
								else {
									// Put callout horizontally in the middle
									var callout_left = (window_width - callout_width) / 2;
									var horizontally_middle = true;
								}

								if (horizontally_middle) {
									if (vertically_bottom) {
										var callout_top = callout_top + hole_height + 20;
									}

									if (vertically_top) {
										var callout_top = callout_top - hole_height - 20;
									}
								}

								// Callout --> Show
								$('#he_callout').css({
									'left': callout_left,
									'top': callout_top,
									'display': 'block'
								});
							}
						}); // end of opening callout on click of an element

						// Cancel button click --> Remove callout and resume hovering
						$(document).on('click', '#he_callout_cancel', function (e) {
							close_callout();
						});

						// Save button --> click
						$(document).on('click', '#he_callout_save', function (e) {

							// Take a screenshot when clicking on a heuristic
							window.hover_enabled = false;
							$('#he_callout').css({
								'left': '0',
								'top': '0',
								'display': 'none'
							});

							// If I don't have the timeout the callout 
							// still appears in the screenshots because 
							// Chrome hasn't finished painting the css change. ugh. 
							setTimeout(function () {
								chrome.runtime.sendMessage({
									greeting: "take_screenshot"
								}, function (response) {
									// console.log('chrome runtime send message called and returned');

									// Show save notification

									$('body').append('<div class="he_notification_container"><div class="he_notification">Screenshot saved</div></div>');
									window.setTimeout(function () {
										$('.he_notification_container').fadeOut(1000, function () {
											$(this).remove();
										});
									}, 2000);

									window.screenshot = response.farewell;
									if (pause_selection == false)
										window.hover_enabled = true;

									// === Crop screenshot ===

									// generate canvas
									$('body').append('<canvas id="crop_canvas" style="display:none; position:fixed; z-index:2147483648;left:0;top:0;"></canvas>');
									var canvas = document.getElementById('crop_canvas');
									var context = canvas.getContext('2d');
									var imageObj = new Image();
									imageObj.src = window.screenshot;

									// Once image is loaded, then we can get dimensions and use it
									imageObj.onload = function () {

										// draw cropped image to canvas
										var sourceX = 274 * window.devicePixelRatio;
										var sourceY = 0;
										var sourceWidth = imageObj.width - sourceX;
										var sourceHeight = imageObj.height;
										var destX = 0;
										var destY = 0;
										canvas.width = sourceWidth;
										canvas.height = sourceHeight;
										context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, sourceWidth, sourceHeight); //, destWidth, destHeight

										// save image & remove canvas
										var cropped_screenshot = canvas.toDataURL("image/jpg");

										$('#crop_canvas').remove();

										// === Save data to json ===
										// Get data from the DOM
										var save_heuristic = $('#ux_heuristic_select').val();
										var save_severity = $('#ux_severity_dropdown').val();
										var save_notes = $('#ux_observations').val();
										var save_recommendation = $('#ux_recommendation').val();
										var save_screenshot = cropped_screenshot;

										// Load previously saved data
										//chrome.storage.local.set({'saved_data': null})
										chrome.storage.local.get('saved_data', function (result) {
											// Unpack results
											saved_data = {}

											if (result.saved_data == undefined) {
												saved_data[localStorage.getItem('current_report_name')] = []
											} else {
												saved_data = result.saved_data;

											}

											// Add data
											hueristic_data = {
												"webpage_title": webpage_title,
												"webpage_url": webpage_url.href,
												"heuristic": save_heuristic,
												"severity": save_severity,
												"notes": save_notes,
												"recommendation": save_recommendation,
												"screenshot": save_screenshot
											}

											if ((localStorage.getItem('evaluation_in_progress') == 'true') && (localStorage.getItem('current_report_name') in saved_data)) {
												saved_data[localStorage.getItem('current_report_name')].push(hueristic_data)
											} else {
												saved_data[localStorage.getItem('current_report_name')] = []
												saved_data[localStorage.getItem('current_report_name')].push(hueristic_data)
												localStorage.setItem('evaluation_in_progress', true);
											}

											// Save to local storage
											chrome.storage.local.set({
												'saved_data': saved_data
											});

											// Close the callout
											close_callout();

										});
									}

								});
							}, 50); // timeout time
						}); // end screenshot save and display message
					}
				}
			}
			sendResponse({
				farewell: "okay"
			});
		});
	// On click of "Stop Evaluation", tell Background.js to open the eval page 
	function stopEvaluation(download_report_id, show_message) {

		$('html').css('overflow', 'scroll')
		var report_id = download_report_id
		if (show_message) {
			var r = window.confirm("This will end your current evaluation, and you won't be able to go back & continue. Press ok if you're sure you're all finished.");
		} else {
			var r = true
		}
		if (r == true) {

			chrome.storage.local.get('saved_data', function (result) {
				// Unpack results

				hide_overlays();
				window.hover_enabled = false
				localStorage.setItem('current_report_name', null);
				localStorage.setItem('evaluation_in_progress', false);
				if (result.saved_data == undefined) {
					var saved_data = [];
				} else {
					if (result.saved_data[report_id] == undefined) {
						var saved_data = [];
					} else {
						var saved_data = result.saved_data[report_id];
					}
				}
				var num_of_heuristics = saved_data.length;


				var pdf_html = '';
				pdf_html += `<div class="container-fluid content">
			<div class="row">
				<div class="col-xs-12 col-sm-6 top-header">
				<img src="assets/images/group-7.png" alt="logo">
				</div>
				<div id='download-pdf' class="col-xs-12 col-sm-6 top-header">
				<a id="download_button" class="btn btn-danger pull-right" title="Download Report"><img id="pdf-downlod-img" src="assets/images/download.png" alt="download"/><span>Download Report</span></a>
				</div>
				<div class="col-xs-12  top-header">
				<h1 style="color:#000; margin-left:0; margin-top:30px;">`
				pdf_html += report_id
				pdf_html += `</h1></div>
			</div>
			<div id="report-content" style="height:auto !important">
			<div class=" seven-cols listing-header hidden-xs hidden-sm">
				<!-- listing-header -->
				<div class="col-md-2 listing-header-name Screenshots">
				<h3>Screenshots</h3>
				</div>
				<div class="col-md-3 listing-header-name">
				<h3>Hueristics</h3>
				</div>
				<div class="col-md-2 listing-header-name">
				<h3>Severity</h3>
				</div>
				<div class="col-md-2 listing-header-name">
				<h3>Observations</h3>
				</div>
				<div class="col-md-3 listing-header-name">
				<h3>Recommendations</h3>
				</div>
			</div>`

				for (i = 0; i < num_of_heuristics; i++) {
					pdf_html += '<div class=" seven-cols listing-data report-listing"><div class="col-md-2  col-xs-12 listing-data-entry Screenshots" data-column="Screenshots">' +
						'<img style=" max-width: 150px;" class="pdf-img" alt="user-img" src="' + saved_data[i].screenshot + '"></div>'

					pdf_html += '<div class="col-md-3 col-xs-12 listing-data-entry" data-column="Hueristics"><span class="medium-font">' +
						saved_data[i].heuristic + '</span></div>'

					pdf_html += '<div class="col-md-2 col-xs-12 listing-data-entry" data-column="Severity"><span class="medium-font">' +
						saved_data[i].severity + '</span></div>'

					pdf_html += '<div  class="col-md-2 col-xs-12 listing-data-entry bold-font" data-column="Observations">' +
						saved_data[i].notes + '</div>'

					pdf_html += '<div class="col-md-3 col-xs-12 listing-data-entry bold-font" data-column="Recommendations">' +
						saved_data[i].recommendation + '</div></div>'

				}
				pdf_html += '</div></div>';

				$('iframe.ux-netsol').remove();
				$('#he_callout').remove();
				$('.ux_overlay').remove();


				$("body").css("cssText", "margin-left: 0 !important; width: 100% !important; position:absolute !important; overflow:scroll !important; cursor: auto !important;");
				chrome.runtime.sendMessage({
					greeting: "stop_evaluation",
					html: pdf_html,
					annotations: num_of_heuristics
				}, function (response) {});
			});
			return true
		}
		return false
	};

	function populateHeuristicsInSidePane(ux_settings_saved_data, ux_custom_rules) {
		var container = jQuery('iframe.ux-netsol').contents()
		$('#ux_heuristic_select').html('')
		if (ux_custom_rules.length < 1)
			$('#custom-principle').hide();
		if (select_rule_type === "Nielsen's 10 Heuristic Principles") {
			var heuristics = ux_settings_saved_data.heuristics_sets[0].heuristics;
		} else {
			var heuristics = ux_custom_rules
		}
		for (var j in heuristics) {
			$('#ux_heuristic_select').append('<option>' + heuristics[j].title + '</option>');
		}
		//appned neilsion rules to side pane
		neilsen_rules = ux_settings_saved_data.heuristics_sets.find((obj => {
			return obj.set_name === "Nielsen's 10 Heuristic Principles"
		})).heuristics

		var li = '';
		for (var i in neilsen_rules) {
			li = li + '<li><h2>' +
				neilsen_rules[i].title + '</h2><p>' +
				neilsen_rules[i].details + '</p></li>'
		}
		container.find('.neilsen-rules-ul-pane').html(li);

		//appned ncustom rules to side pane
		custom_rules = ux_custom_rules
		var li = '';
		for (var i in custom_rules) {
			li = li + '<li><h2>' +
				custom_rules[i].title + '</h2><p>' +
				custom_rules[i].details + '</p></li>'
		}
		container.find('.custom-rules-ul-pane').html(li);

	}

	function close_callout() {
		$('#he_callout').css({
			'left': '0',
			'top': '0',
			'display': 'none'
		});
		if (pause_selection == false)
			window.hover_enabled = true;
		$('#ux_severity_dropdown').val("0");
		$('.ui-autocomplete-input').val("");
		$('#ux_observations').val("");
		$('#ux_recommendation').val("");

	}

	function hide_overlays() {
		$("#hover_overlay_top").css({
			"top": "0",
			"left": "0",
			"width": "0px",
			"height": "0px"
		});
		$("#hover_overlay_left").css({
			"top": "0",
			"left": "0",
			"width": "0px",
			"height": "0px"
		});
		$("#hover_overlay_right").css({
			"top": "0",
			"left": "0",
			"width": "0px",
			"height": "0px"
		});
		$("#hover_overlay_bottom").css({
			"top": "0",
			"left": "0",
			"width": "0px",
			"height": "0px"
		});
	}
});