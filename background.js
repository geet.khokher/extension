// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

window.evaluation_in_progress = false;
// If browser action is clicked

chrome.browserAction.onClicked.addListener(function () {
  if (window.evaluation_in_progress == false) {
    window.evaluation_in_progress = true;
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {
        greeting: "please open the tray"
      }, function (response) {});
    });
  } else {
    window.evaluation_in_progress = false;
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {
        greeting: "please close the tray"
      }, function (response) {});
    });
  }
});



// close the extension on page refresh
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete') {
    if (window.evaluation_in_progress == true) {
      window.evaluation_in_progress = false;
      chrome.tabs.query({
        active: true,
        currentWindow: true
      }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
          greeting: "please close the tray"
        }, function (response) {});
      });
    }

  }
})
// Invoke tray
// From popup.js
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    // console.log('background.js recieved the request, ' + request.greeting);

    // Close button clicked, evaluation stopped

    if (request.greeting == "close_button_clicked") {
      window.evaluation_in_progress = false;
    }

    // Stop evaluation

    if (request.greeting == "stop_evaluation") {
      window.evaluation_html = request.html;
      chrome.tabs.create({
        url: 'evaluationpage.html'
      });
      window.evaluation_in_progress = false;

      chrome.tabs.query({
        active: true,
        currentWindow: true
      }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
          greeting: "stopping_evalution"
        }, function (response) {

        });
      });
    }

    // Take screenshot
    if (request.greeting == "take_screenshot") {
      chrome.tabs.captureVisibleTab(null, {}, function (dataUrl) {
        window.screenshot = dataUrl;
        // console.log(window.screenshot);
        sendResponse({
          farewell: window.screenshot
        });
      });
      return true;
    }

    // Pass evaluation info to evaluation page

    if (request.greeting == "gimme_evaluation_info") {
      sendResponse({
        greeting: "heres_ur_data",
        evalInfo: window.evaluation_html
      });
    }


  }
);