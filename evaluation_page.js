$(document).ready(function () {
	chrome.runtime.sendMessage({
		greeting: "gimme_evaluation_info"
	}, function (response) {

		$('body').html(response.evalInfo);
		$('body').append('<div id="editor"></div>')


		$(document).on('click', '#download_button', function () {
			$('#download-pdf').hide()
			$('#pdf-downlod-img').hide()


			var h = $('.listing-data').length + 4
			var doc = new jsPDF({
				orientation: 'portrait',
				unit: 'in',
				format: [9, h]
			})
			doc.addHTML(document.body, function () {

				doc.save('Audit Report.pdf');
			});
			$('#download-pdf').show()
			$('#pdf-downlod-img').show()

		})
	});
});